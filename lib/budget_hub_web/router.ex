defmodule BudgetHubWeb.Router do
  use BudgetHubWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BudgetHubWeb do
    pipe_through :api
  end
end
