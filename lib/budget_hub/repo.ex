defmodule BudgetHub.Repo do
  use Ecto.Repo,
    otp_app: :budget_hub,
    adapter: Ecto.Adapters.Postgres
end
