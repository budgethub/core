# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :budget_hub,
  ecto_repos: [BudgetHub.Repo]

# Configures the endpoint
config :budget_hub, BudgetHubWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "iwx6y1PddEnUIpclLSc6M8Cx4yf9mqoCyfIDq1JNfMgyU5xV9YOEh+o3V2P2ALZQ",
  render_errors: [view: BudgetHubWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: BudgetHub.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
