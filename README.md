# BudgetHub

## Overview

- [BudgetHub](https://www.budgethub.xyz) is an open source budgeting platform.
- This project provides the [BudgetHub API](https://www.budgethub.xyz) for consumption.
- A public deployment of this code is maintained by BudgetHub.

## Tech Stack

- Language: [Elixir](https://elixir-lang.org/)
- Web Framework: [Phoenix](http://phoenixframework.org/)
- Database: [PostgreSQL](https://www.postgresql.org/)

## How to contribute

* Fork this repository
* Write your awesome feature/fix/refactor
* Create a merge request
* Wait for your MR to be reviewed and merged
* You're now a contributor!

For more information, check out our [guidelines for contributing](CONTRIBUTING.md).

## Quick Start

```
git clone https://gitlab.com/budgethub/core.git
cd core
mix deps.get
mix ecto.create && mix ecto.migrate
mix phx.server
```

Afterwards, open your favorite REST client and go to [localhost:4000/api](http://localhost:4000/api)
